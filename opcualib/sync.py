# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

"""Synchronous PLC interface"""

# Built-in
import asyncio
import functools
import logging
import datetime as dt
import concurrent.futures
import time
import numbers

# PyPI
from asyncua import ua, sync
from asyncua.ua.uaerrors import BadNoMatch


class PLCTimeoutError(concurrent.futures.TimeoutError):
    """Raised when PLC connection is lost for some time"""


class TimeoutThreadLoop(sync.ThreadLoop):
    """Wait for the future with a timeout"""

    def __init__(self, timeout=None):
        super().__init__()
        self.timeout = timeout
        self.name = "ThreadLoopOpcClient"
        self.daemon = True

    def post(self, coro):
        if not self.loop or not self.loop.is_running():
            raise sync.ThreadLoopNotRunning(f"could not post {coro}")
        futur = asyncio.run_coroutine_threadsafe(coro, loop=self.loop)
        try:
            return futur.result(self.timeout)
        except (concurrent.futures.TimeoutError, AttributeError):
            # The Attribute error may happen if the transport object
            # in asyncio protocol is set to None.
            futur.cancel()
            time.sleep(0.01)
            raise PLCTimeoutError from None


class SyncClient(sync.Client):
    """Synchronous opcua client for communicating with the PLC

    A thread will be started when an instance of this class is
    created. This thread will handle the communication of requests to
    the underlying asyncio event loop. It is important to stop this
    thread when stopping the application in order to not hang
    indefinitely. The proper to do this is to use the `shutdown`
    method or `__exit__` when used as a context manager. However, you
    don't need to call `shutdown` when `connect` fails, it will stop
    the loop for you in that (only) case.

    When the PLC connection is lost, for example, the PLC is
    restarted or ethernet cable unplugged, the `PLCTimeoutError`
    exception is raised.

    Use as a context manager:

    >>> with SyncClient() as client:
    >>>     value = client.get_float('HV_VoltageDemand_GET')
    >>>     print(value)

    Or the procedural way:

    >>> client = SyncClient()
    >>> client.connect()
    >>> value = client.get_float('HV_VoltageDemand_GET')
    >>> print(value)
    >>> client.shutdown()

    The public methods are:

        Start/stop:
        * connect
        * shutdown

        Subscriptions:
        * subscribe

        Getters:
        * get_value - regardless of type
        * get_as_dict
        * get_int
        * get_uint
        * get_float
        * get_double
        * get_bool
        * get_string
        * get_struct

        Setters:
        * set_value - regardless of type
        * set_int
        * set_uint
        * set_float
        * set_double
        * set_bool
        * set_string
        * set_struct
    """

    NAMESPACE_MAPPER = {
        "http://opcfoundation.org/UA/": [
            "Root",
            "Objects",
            "Server",
            "ServerStatus",
            "CurrentTime",
        ],
        "http://PLCopen.org/OpcUa/IEC61131-3/": ["GlobalVars"],
    }
    PATH_DELIMITER = "|"

    def __init__(
        self,
        hostname: str = "127.0.0.1",
        port: int = 4840,
        namespace: str = "",
        base: str = "",
        url_fmt: str = "opc.tcp://{hostname}:{port}/",
        timeout: int = 4,
    ):
        url = url_fmt.format(hostname=hostname, port=port)
        tloop = TimeoutThreadLoop(timeout)
        tloop.start()
        self.close_tloop = True
        super().__init__(url, timeout, tloop)
        self.hostname = hostname
        self.port = port
        self.base = base
        self.namespace = namespace
        self.namespace_idx = 0  # Used as default
        self._object_namespace_mapper = dict()
        self._root_node = None
        self.path_transform = lambda s: s
        self.log = logging.getLogger("SyncClient")

    def connect(self):
        """Create connection to the PLC.

        The timeout on connect failure can not easily be changed.
        Currently, it is about 30 seconds.
        """
        self.log.info("Connecting to PLC ...")
        try:
            super().connect()
        except (KeyboardInterrupt, Exception) as error:
            # Typical error message when ethernet cable is not connected:
            #   [Errno 110] Connect call failed ('192.168.100.20', 4840)
            # Typical error message when Ctrl+C is pressed:
            #   ERROR:asyncio:Task was destroyed but it is pending!
            self.tloop.stop()
            self.tloop.join()
            raise error
        else:
            self.log.info("Connected to PLC.")

        # Collect namespace indexes and get root node
        try:
            self.log.debug("Collecting namespace indexes.")
            self._collect_namespace_idx()
            self.log.debug("Getting root node.")
            self._root_node = self.get_root_node()
        except (KeyboardInterrupt, Exception):
            self.shutdown()
            raise error

    def __enter__(self):
        self.connect()
        return self

    def shutdown(self):
        """Kill the client connection, internal threads and event loop"""
        # _change_timeout(5)  # Used to speed up disconnect when timeout
        self.log.info("Disconnecting OPC UA client ....")
        try:
            self.disconnect()
        except PLCTimeoutError:
            # Timeout indicates that there is no connection. In that
            # case it is not possible to communicate the `disconnect`
            # to the PLC and we have no other option than to just stop
            # the loop.
            self.log.debug("Timeout when disconnecting.")
        else:
            self.log.info("Disconnecting OPC UA client successful.")
        finally:
            # This is safe to do even when `disconnect` was successful
            self.log.debug("Stopping loop.")
            self.tloop.stop()
            self.tloop.join()

    def __exit__(self, exc_type, exc_value, traceback):
        self.shutdown()

    def subscribe(self, path, handler, period_ms=100):
        """Create a new subscription to a OPCUA variable"""
        sub = None
        try:
            node = self.get_node_implicit(path)

            sub = super().create_subscription(period_ms, handler)
            sub.subscribe_data_change([node])
        except Exception as error:
            self.log.error("Failed subscribing to path: %r", path)
            raise error

        return sub

    def _collect_namespace_idx(self):
        """Retrieve the current namespace indices we're interested in"""
        self.namespace_idx = self.get_namespace_index(self.namespace)
        for namespace, objects in self.NAMESPACE_MAPPER.items():
            idx = self.get_namespace_index(namespace)
            for obj in objects:
                self._object_namespace_mapper[obj] = idx
        self.log.debug("Object namespace mapper: %r", self._object_namespace_mapper)

    def _namespace_idx_for_object(self, object_name):
        return self._object_namespace_mapper.get(object_name, self.namespace_idx)

    def _assemble_path(self, path: str) -> str:
        """Join `path` with a `base` path and convert to OPC-UA nomenclature

        Example output:
          ['0:Objects', '4:JC2', '3:GlobalVars', '4:HV_Start_CMD']

        """
        # Assuming base path is of form `Objects|Object|Variable`
        p = self.base.split(self.PATH_DELIMITER) + path.split(self.PATH_DELIMITER)
        # Collect the path in a form the opcua lib understands.
        return tuple(f"{self._namespace_idx_for_object(x)}:{x}" for x in p)

    def get_root_node(self):
        return self.get_node(ua.TwoByteNodeId(ua.ObjectIds.RootFolder))

    @functools.lru_cache()
    def get_node_explicit(self, full_path):
        try:
            node = self._root_node.get_child(full_path)
        except BadNoMatch as error:
            # Improve error message by including the variable path
            exc = BadNoMatch(
                f"BadNoMatch on {full_path!r}. "
                f"Does the variable exist and also spelled correctly?"
            )
            raise exc from error
        return node

    def get_node_implicit(self, path: str):
        path = self.path_transform(path)
        full_path = self._assemble_path(path)
        return self.get_node_explicit(full_path)

    # --- Get node value

    def get_value(self, path: str, expected_type=None, log=True):
        try:
            path = self.path_transform(path)
            full_path = self._assemble_path(path)
            var = self.get_node_explicit(full_path)
            value = var.get_value()
        except Exception as error:
            self.log.error("Failed when getting variable: %r", full_path)
            raise error
        else:
            if log:
                self.log.debug("GET %s -> %r", path, value)
        if expected_type is not None and not isinstance(value, expected_type):
            raise TypeError(
                f"Received value {value!r} has type {type(value)!r} but "
                f"{expected_type!r} was expected. Variable: {full_path!r}."
            )
        return value

    def get_as_dict(self, paths: list) -> dict:
        output = dict()
        for path in paths:
            output[path] = self.get_value(path)
        return output

    def get_int(self, path) -> int:
        """Updates the int value at `path`. Converts to Int64 value"""
        return self.get_value(path, int)

    get_uint = get_int

    def get_float(self, path) -> float:
        """Updates the float value at `path`"""
        return self.get_value(path, numbers.Real)

    def get_double(self, path) -> float:
        """Updates the double value at `path`"""
        return self.get_value(path, numbers.Real)

    def get_bool(self, path) -> bool:
        """Updates the bool value at `path`"""
        return self.get_value(path, bool)

    def get_string(self, path) -> str:
        """Updates the string value at `path`"""
        return self.get_value(path, str)

    def get_datetime(self, path) -> dt.datetime:
        """Updates the string value at `path`"""
        return self.get_value(path, dt.datetime)

    def get_struct(self, path):
        """Updates the struct value at `path`."""
        return self.get_value(path)

    # --- Set node value

    def set_value(self, path: str, value, type_: ua.VariantType):
        try:
            path = self.path_transform(path)
            self.log.debug("SET %s <- %r (%r)", path, value, type_.name)
            full_path = self._assemble_path(path)
            var = self.get_node_explicit(full_path)
            datavalue = ua.DataValue(ua.Variant(value, type_))
            var.set_value(datavalue)
        except Exception as error:
            self.log.error("Failed when setting variable: %r", full_path)
            raise error

    def set_int(self, path, value):
        """Updates the int value at `path`. Converts to Int64 value"""
        self.set_value(path, value, ua.VariantType.Int64)

    def set_uint(self, path, value):
        """Updates the int value at `path`. Converts to UInt64 value"""
        self.set_value(path, value, ua.VariantType.UInt64)

    def set_float(self, path, value):
        """Updates the float value at `path`"""
        self.set_value(path, value, ua.VariantType.Float)

    def set_double(self, path, value):
        """Updates the double value at `path`"""
        self.set_value(path, value, ua.VariantType.Double)

    def set_bool(self, path, value):
        """Updates the bool value at `path`"""
        self.set_value(path, value, ua.VariantType.Boolean)

    def set_string(self, path, value):
        """Updates the string value at `path`"""
        self.set_value(path, value, ua.VariantType.String)

    def set_struct(self, path, value):
        """Updates the struct value at `path`.

        # TODO: Copy-pasted method from previous wrapper

        The value needs to be a properly initialized ExtensionObject.
        Preferaby created using a convenience method, e.g
        `layerfeed_object()`
        """
        self.set_value(path, value, ua.VariantType.ExtensionObject)


def mock_syncclient(get_values):
    import unittest
    from unittest.mock import MagicMock, patch

    opc = MagicMock(spec=SyncClient)
    call_count = dict()

    def fake_call(key):
        index = call_count.setdefault(key, 0)
        values = get_values[key]
        if not isinstance(values, list):
            return values
        try:
            output = values[index]
        except IndexError:
            output = values[-1]
        call_count[key] += 1
        return output

    opc.get_int.side_effect = fake_call
    opc.get_float.side_effect = fake_call
    opc.get_double.side_effect = fake_call
    opc.get_bool.side_effect = fake_call
    opc.get_string.side_effect = fake_call
    opc.get_datetime.side_effect = fake_call
    opc.get_struct.side_effect = fake_call
    return opc
