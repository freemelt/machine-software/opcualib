#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

# Built-in
import asyncio
import logging
import socket
import threading
import functools
import concurrent.futures
import warnings

# PyPI
from asyncua import Client, Node, ua
from asyncua.ua import uaerrors
from asyncua.ua.uatypes import ExtensionObject
from asyncua.ua.uatypes import NodeId, NodeIdType
import asyncua
import time


class SubCallbacker(object):
    def __init__(self):
        # Maps nodestring -> array of callbacks, e.g: Objects|new_Controller_2|global_vars|Something -> [def callback(nodestring, value), ...]
        self._callback_mapper = {}
        self._node_path_mapper = {}

    def datachange_notification(self, node: Node, val, notification_data):
        # We can not make async calls in this callback
        # THIS METHOD IS UNVERIFIED TO WORK WITH OPC CALLS
        logging.getLogger("OPCWrapper").debug(
            f" !! datachange_notification {node} {val}"
        )
        if node.nodeid in self._callback_mapper:
            for callback in self._callback_mapper[node.nodeid]:
                node_path = self._node_path_mapper[node.nodeid]
                # Schedule callback on event loop
                # Since this callback is on the main thread
                # The loop on the main thread will be used
                logging.getLogger("OPCWrapper").debug(
                    f" executing on worker thread: START {threading.current_thread()}"
                )
                with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
                    future = executor.submit(
                        functools.partial(callback, node_path, val, notification_data)
                    )
                    self.log.debug(f"Result: {future.result()}")
                # asyncio.get_event_loop().call_soon_threadsafe(functools.partial(callback, node_path, val, notification_data))  # Let the world know!
                logging.getLogger("OPCWrapper").debug(
                    f" executing on worker thread: COMPLETE"
                )

    def add(self, nodeid, node_path, callback):
        self._node_path_mapper[nodeid] = node_path
        if nodeid not in self._callback_mapper:
            self._callback_mapper[nodeid] = [callback]
        else:
            callback_ary = self._callback_mapper[nodeid]
            callback_ary.append(callback)


class OPCWorker(
    threading.Thread,
):
    def __init__(self, opc_wrapper):
        super(OPCWorker, self).__init__()  # Init threading
        self.opc = opc_wrapper

    # Thread related
    def stop(self):
        """Stop the OPCWrapper thread"""
        self.opc.complete()

    def run(self):
        """Threading thread. Blocking call for run loop"""
        self.opc.log.info("Starting thread")
        self.opc.wait_until_complete()

    # Thread related


class OPCWrapper(object):
    """A wrapper around FreeOpcUa opcua-asyncio lib

    *Deprecated* and replaced with opcualib.SyncClient which uses a
    Newer and better upstream version of asyncua (0.6.1).

    This lib simplifies the API and adds fault tolerant
    behavior such as automatic reconnection on
    unwanted connection loss.
    """

    # This will be used to translate an sub-path into
    # its proper namespace index for known OPC UA objects
    NAMESPACE_MAPPER = {
        "http://opcfoundation.org/UA/": [
            "Root",
            "Objects",
            "Server",
            "ServerStatus",
            "CurrentTime",
        ],
        "http://PLCopen.org/OpcUa/IEC61131-3/": ["GlobalVars"],
    }

    PATH_DELIMITER = "|"

    def __init__(self, opc_hostname, opc_port=4840):
        warnings.warn(
            "OPCWrapper is deprecated. Use SyncClient instead.", DeprecationWarning
        )
        self.log = None
        self.debug_level = logging.DEBUG
        self.log = logging.getLogger("OPCWrapper")
        self.log.setLevel(self.debug_level)

        self._is_connected = False
        self._wants_connection = False

        self._hostname = opc_hostname
        self._port = opc_port
        self._conn = None
        self._endpoint = ""  # << TBD
        self._namespace = ""
        self._namespace_idx = 0  # TBD if this will be used
        self._root_node = None
        self._base_path = ""

        self._loop = asyncio.get_event_loop()  # Schedule everything on main run loop
        self.wrap = self._threader

        # Maps an object to it's namespace index
        self._object_namespace_mapper = {}

        # Subscriptions
        self._subscription = None
        self._subcallbacker = SubCallbacker()

        # Silence Free opcua lib logger
        logging.getLogger("asyncio").setLevel(logging.WARN)
        logging.getLogger("asyncua").setLevel(logging.WARN)
        logging.getLogger("asyncua.server.uaprocessor").setLevel(logging.WARN)

    def _threader(self, func):
        """This function will abstract away running on multiple threads."""
        if not self._loop.is_running():
            return self._loop.run_until_complete(func)

        f = asyncio.run_coroutine_threadsafe(func, self._loop)
        return f.result(2.0)  # 2 seconds timeout

    async def _reconnect_check(self):  # TBD if this is necessary
        """Use to keep connections running.

        Checks for missing connection every second.
        TBD if this interval should be configurable.
        """
        # self.log.debug("Reconnect check")
        delay = 1.8  # After we've connected
        if self._wants_connection == True and self._is_connected == False:
            # Re-establish connection
            try:
                await self._connect()
                # TODO: Also restore subscriptions

                self.log.info(" > re-establishing...")
            except socket.gaierror as e:
                self.log.debug(" > no such host")
            except ConnectionRefusedError as e:
                self.log.debug(" > could not connect, refused")
            except Exception as e:
                self.log.debug(f" > could not reconnect: {e}")

            if self._is_connected == False:

                delay = 0.7  # Reconnect time
            else:
                self.log.info(" ===================== ")
                self.log.info(" ===[ RECONNECTED ]=== ")
                self.log.info(" ===================== ")

        # Re-schedule next connection check
        if self._wants_connection:
            self._loop.call_later(
                delay, lambda: asyncio.ensure_future(self._reconnect_check())
            )

    def __del__(self):
        if self._subscription is not None:
            self.wrap(self._subscription.delete())
        if self.log is not None:
            self.log.debug("Being deleted.. Bye bye")
        self._loop.close()  # Dont mess the event loop up for others

    def _did_disconnect(self, exc):
        self.log.debug(" DID DISCONNECT ")

        # Call the original method first
        self._conn.uaclient.protocol.swizzled_connection_lost(exc)
        self._is_connected = False

    def _reconnector(func):
        def reconnect_wrapper(self, *args, **kwargs):
            delay = 0.5  # Start waiting time
            counter = 0  # Wait counter
            while self._wants_connection == True and self._is_connected == False:
                self.log.debug(" Awaiting connection ...")
                if self._is_connected == False:
                    self.wrap(asyncio.sleep(delay))
                    counter = counter + 1  # TODO: Use for increasing delay

            return func(self, *args, **kwargs)  # Only do this if we have a connection

        return reconnect_wrapper

    async def _connect(self):
        # Init opc ua connection
        server_url = f"opc.tcp://{self._hostname}:{self._port}/{self._endpoint}"

        self.log.debug(f"Trying {server_url}")
        self._conn = asyncua.Client(url=server_url)
        self.log.debug(f"client created")

        # self.wrap( self._conn.connect() )
        await self._conn.connect()
        self.log.debug(f"passed connect")
        self._is_connected = True

        # Swizzle callback
        self._conn.uaclient.protocol.swizzled_connection_lost = (
            self._conn.uaclient.protocol.connection_lost
        )
        self._conn.uaclient.protocol.connection_lost = self._did_disconnect

        self._root_node = self._conn.get_root_node()
        await self._collect_namespace_indices()

    async def _collect_namespace_indices(self):
        """Retrieve the current namespace indices we're interested in"""

        self.log.debug("Retrieving known OPC namespace indices")

        if self._namespace != "":
            self._namespace_idx = await self._conn.get_namespace_index(self._namespace)

        for namespace, objects in OPCWrapper.NAMESPACE_MAPPER.items():
            idx = 0
            try:
                idx = await self._conn.get_namespace_index(namespace)
            except ValueError as e:
                # Namespace does not exist on this server
                self.log.warning(e)
                continue
            except:
                pass

            for o in objects:
                self._object_namespace_mapper[o] = idx

    def _namespace_idx_for_object(self, object_name):
        idx = self._namespace_idx  # Default
        if object_name in self._object_namespace_mapper:
            idx = self._object_namespace_mapper[object_name]
        return idx

    def subscribe_to(self, node_path, callback):
        """Callback for data updated/changed event

        def callback(node, value, notification_data)

        Do not do anything time consuming here.
        Preferably only store the received value.
        The callback is executed on the main thread,
        do not call any OPC-related commands in there.
        """

        node_ary = self._assemble_path(node_path)
        self.log.debug(f"Subscribing to {self._full_string_path(node_path)}")
        if self._subscription is None:
            self._subscription = self.wrap(
                self._conn.create_subscription(500, self._subcallbacker)
            )

        node = self.wrap(self._root_node.get_child(node_ary))
        self.wrap(self._subscription.subscribe_data_change([node]))

        # Register our callbacks
        self._subcallbacker.add(node.nodeid, node_path, callback)

    def unsubscribe_from(self, node_path, callback):
        """Remove a single subscription calback"""
        self.log.debug(f"unsubscribe_from {node_path} - {callback}")
        node_ary = self._assemble_path(node_path)
        node = self.wrap(self._root_node.get_child(node_ary))
        if node.nodeid in self._callback_mapper:
            if callback in self._callback_mapper[node.nodeid]:
                self.log.debug(f" > Found! Removing")
                ary = self._callback_mapper[node.nodeid]
                ary.remove(callback)

    def connect(self):
        """Connects to a OPC-UA server with parameters from the __init__ method."""

        self.log.debug("Connecting...")

        self.wrap(self._connect())
        self._wants_connection = True
        self._is_connected = True

        # Continuously check and restore connection
        # Start the loop of checking reconnections
        self.wrap(self._reconnect_check())

    def is_connected(self):
        """Returns current connection status"""
        return self._is_connected

    def disconnect(self):
        """Disconnects the current connection"""

        self.log.debug("Disconnecting..")
        self._wants_connection = False
        self._is_connected = False
        self.wrap(self._conn.disconnect())

    def set_variables_namespace(self, namespace):
        self.log.info(f"Setting new variables namespace: {namespace}")
        self._namespace = namespace
        if namespace != "" and self.is_connected() == True:
            self.wrap(self._collect_namespace_indices())

    def set_base_path(self, path):
        """Sets an optional OPC-UA root path for subsequent get and set calls."""
        self.log.debug(f"Setting base path {path}")
        self._base_path = path

    def set_debug_level(self, lvl):
        """Sets the debug level output for the opcua lib and local prints to stdout. DEBUG is default."""
        self.log.debug(f"Changing debug level to {lvl}")
        self.log.setLevel(lvl)

    def wait_until_complete(self):
        """This starts the internal asyncio event loop. Blocking.

        MUST CALL if multi threaded application, or if the application
        is not running on the main thread.
        """
        self._loop.run_forever()

    def complete(self):
        """Called when we need to shut down. Ends the wait_until_complete() call."""
        # self.disconnect()
        self._loop.stop()

    def layerfeed_object(self, body=None):
        """Correct type for a layerfeed object

        This is an object specific for Freemelt, breaks generic
        opcua interface.
        """
        ext = ExtensionObject()
        # The namespace idx is assumed to the same for the
        # extension object as it is for the variables
        # 5002 is the extension object type used for LayerFeedParams
        ext.TypeId = NodeId(
            namespaceidx=self._namespace_idx,
            identifier=5002,
            nodeidtype=NodeIdType.FourByte,
        )
        ext.Body = body
        ext.Encoding = 1
        return ext

    #
    # Setters
    #
    def set_int(self, path, value):
        """Updates the int value at `path`. Converts to Int64 value"""
        self._set_value(path, value, ua.VariantType.Int64)

    def set_float(self, path, value):
        """Updates the float value at `path`"""
        self._set_value(path, value, ua.VariantType.Float)

    def set_double(self, path, value):
        """Updates the double value at `path`"""
        self._set_value(path, value, ua.VariantType.Double)

    def set_bool(self, path, value):
        """Updates the bool value at `path`"""
        self._set_value(path, value, ua.VariantType.Boolean)

    def set_string(self, path, value):
        """Updates the string value at `path`"""
        self._set_value(path, value, ua.VariantType.String)

    def set_struct(self, path, value):
        """Updates the struct value at `path`.

        The value needs to be a properly initialized ExtensionObject.
        Preferaby created using a convenience method, e.g `layerfeed_object()`
        """
        # self.log.debug(f"{value} - {type(value)} - {dir(value)} - {vars(value)}")
        self._set_value(path, value, ua.VariantType.ExtensionObject)

    #
    # Getters
    #
    def get_int(self, path):
        """Retrieves the int value at `path`"""
        return self._get_value(path, int)

    def get_float(self, path):
        """Retrieves the float value at `path`"""
        return self._get_value(path, float)

    def get_bool(self, path):
        """Retrieves the bool value at `path`"""
        return self._get_value(path, bool)

    def get_string(self, path):
        """Retrieves the string value at `path`"""
        return self._get_value(path, str)

    def get_struct(self, path):
        """Retrieves the struct value at `path`. Not implemented."""
        return self._get_value(path)

    #
    # Internal functions
    #
    def _full_string_path(self, path):
        """Convenience function to get a full OPC variable string path, e.g"""
        return (
            self._base_path.strip(OPCWrapper.PATH_DELIMITER)
            + OPCWrapper.PATH_DELIMITER
            + path.strip(OPCWrapper.PATH_DELIMITER)
        )

    def _assemble_path(self, path):
        """Assemble the `path` with a `root` path and convert to OPC-UA nomenclature"""
        # Assuming root path is of form `Objects|Object|Variable`, and same for path
        path_ary = []
        if len(self._base_path) != 0:
            path_ary.extend(self._base_path.split(OPCWrapper.PATH_DELIMITER, -1))
        if len(path) != 0:
            path_ary.extend(path.split(OPCWrapper.PATH_DELIMITER, -1))

        # Collect the path in a form the opcua lib understands
        ary = [f"{self._namespace_idx_for_object(x)}:{x}" for x in path_ary]
        print(ary)
        return ary

    @_reconnector
    def _set_value(self, path, value, val_type):
        # self.log.debug(f"_set_value {path}({value}) of type {val_type}")
        p = self._assemble_path(path)

        # Execute write command
        keep_trying = True
        while keep_trying == True:  # TODO: Add counter
            keep_trying = False
            try:
                # Retrieve node object
                var = self.wrap(self._root_node.get_child(p))
                # Convert to DataValue manually
                datavalue = ua.DataValue(ua.Variant(value, val_type))
                self.wrap(var.set_value(datavalue))
                # self.log.debug(f"Write {var} to {path} with value '{value}'")
            except asyncio.TimeoutError as e:
                self.log.debug(f" TIMEOUT ERROR while SET {path} to {value}")
                keep_trying = True
            except uaerrors.BadNoMatch as e:
                self.log.debug(
                    f" {self._full_string_path(path)} [ {' | '.join(p)} ] does not exist! could not SET {value}: {e}"
                )
                raise
            except Exception as e:
                self.log.debug(f"set '{path}' value general error: {e}")

    @_reconnector
    def _get_value(self, path, val_type=None):
        p = self._assemble_path(path)

        # Execute read command
        value = None
        keep_trying = True
        while keep_trying == True:  # TODO: Add counter
            keep_trying = False
            try:
                # Retrieve node object
                var = self.wrap(self._root_node.get_child(p))
                value = self.wrap(var.get_value())
                if val_type is not None:
                    value = val_type(value)  # Dynamic type casting
                # self.log.debug(f"Received {path} with value '{value}'")
            except asyncio.TimeoutError as e:
                self.log.debug(f" TIMEOUT ERROR while GET {path}")
                keep_trying = True
            except uaerrors.BadNoMatch as e:
                self.log.debug(
                    f" {self._full_string_path(path)} [ {' | '.join(p)} ] does not exist! could not GET : {e}"
                )
                raise
            except Exception as e:
                self.log.debug(f"get '{path}' value general error: {e}")

        if val_type is not None and type(value) is not val_type:
            raise Exception(
                f"expected type mismatch: {path} -> '{value}' is type {type(value)} and not requested type {val_type} "
            )

        return value


OPC_LOCK = threading.Lock()


class OpcVar:
    def __init__(self, cfg, opc):
        self.cfg = cfg
        self.opc = opc

    def __getattr__(self, method):
        m = getattr(self.opc, method)

        @functools.wraps(m)
        def wrapper(path, *args, **kwargs):
            with OPC_LOCK:
                return m(self.cfg[path], *args, **kwargs)

        return wrapper
