# SPDX-FileCopyrightText: 2019-2023 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only
"""Simulation of OPC UA PLC interface through shared memory

This module provides a "fake" version of opcualib.sync.SyncClient
which can be used to attach to a simulated PLC.
"""

# Built-in
import copy
import datetime as dt
import logging
import numbers
import pathlib
import threading
from typing import Optional

# Third-party
from asyncua import ua

# Project
from .variables import OpcUa


def extract_leafs(prefix: str, data: dict) -> list[tuple[str, str]]:
    """Extract all leafs and keypath

    >>> extract_leafs("", {"a": {"b": {"c": "mystring"}, "d": "asdf"}})
    [('a:b:c', 'mystring'), ('a:d', 'asdf')]
    """
    leaves = list()
    if isinstance(data, dict):
        for key, value in data.items():
            if isinstance(value, dict):
                leaves.extend(extract_leafs(f"{prefix}:{key}", value))
            elif isinstance(value, str):
                leaves.append((f"{prefix}:{key}".lstrip(":"), value))
    elif isinstance(data, str):
        leaves.append((prefix.lstrip(":"), data))
    return leaves


class Subscription:
    def __init__(self, stop_event: threading.Event) -> None:
        """Initialization of Subscription"""
        self.stop_event = stop_event

    def delete(self) -> None:
        self.stop_event.set()


class SyncClient(OpcUa):
    """Simulated opcualib.sync.SyncClient through shared memory"""

    def __init__(
        self,
        config: dict,
        name: str = "opcualib",
        lockfile: Optional[pathlib.Path] = None,
    ) -> None:
        super().__init__(name=name, lockfile=lockfile, owner=False)
        cfg = copy.deepcopy(config)
        del cfg["Namespace"]  # Not useful with extract_leafs
        del cfg["Base"]  # Not useful with extract_leafs

        self.aliases = dict()
        for alias, name in extract_leafs("", cfg):
            self.aliases[alias] = name

        self.namespace_idx = 0
        self.log = logging.getLogger("SyncClient")

    def _monitor(self, sub: Subscription, path: str, handler, period_ms: int) -> None:
        # Fake subscription by polling
        try:
            x0 = self.get_value(path)
            while not sub.stop_event.is_set():
                x1 = self.get_value(path)
                if x1 != x0:
                    handler.datachange_notification(path, x1, None)
                    x0 = x1
                sub.stop_event.wait(period_ms / 1_000)
        except Exception:
            self.log.exception("Subscription error for %s", path)

    def subscribe(self, path: str, handler, period_ms: int = 100) -> Subscription:
        """Create a new subscription to an OPCUA variable"""
        sub = Subscription(threading.Event())
        t1 = threading.Thread(
            daemon=True,
            target=self._monitor,
            args=(sub, path, handler, period_ms),
        )
        t1.start()
        return sub

    # --- Get node value

    def get_value(self, path: str, expected_type=None, log=True):
        try:
            value = self.get(self.aliases[path])
            if expected_type == int and isinstance(value, float):
                value = int(value)
        except Exception as error:
            self.log.error("Failed when getting variable: %r", path)
            raise error
        if expected_type is not None and not isinstance(value, expected_type):
            raise TypeError(
                f"Received value {value!r} has type {type(value)!r} but "
                f"{expected_type!r} was expected. Variable: {path!r}."
            )
        return value

    def get_as_dict(self, paths: list) -> dict:
        output = dict()
        for path in paths:
            output[path] = self.get_value(path)
        return output

    def get_int(self, path: str) -> int:
        return self.get_value(path, int)

    get_uint = get_int

    def get_float(self, path: str) -> float:
        return self.get_value(path, numbers.Real)

    def get_double(self, path: str) -> float:
        return self.get_value(path, numbers.Real)

    def get_bool(self, path: str) -> bool:
        return self.get_value(path, bool)

    def get_string(self, path: str) -> str:
        return self.get_value(path, str)

    def get_datetime(self, path: str) -> dt.datetime:
        return self.get_value(path, dt.datetime)

    def get_struct(self, path: str):
        class Dummy:
            pass

        d = Dummy()
        d.Body = self.get_value(path)
        return d

    # --- Set node value

    def set_value(self, path: str, value, type_: ua.VariantType):
        try:
            self.set(self.aliases[path], value)
        except Exception as error:
            self.log.error("Failed when setting variable: %r", path)
            raise error

    def set_int(self, path: str, value: int):
        self.set_value(path, value, ua.VariantType.Int64)

    def set_uint(self, path: str, value: int):
        self.set_value(path, value, ua.VariantType.UInt64)

    def set_float(self, path: str, value: float):
        self.set_value(path, value, ua.VariantType.Float)

    def set_double(self, path: str, value: float):
        self.set_value(path, value, ua.VariantType.Double)

    def set_bool(self, path: str, value: bool):
        self.set_value(path, value, ua.VariantType.Boolean)

    def set_string(self, path: str, value):
        self.set_value(path, value, ua.VariantType.String)

    def set_struct(self, path: str, value):
        self.set_value(path, value.Body, ua.VariantType.ExtensionObject)
