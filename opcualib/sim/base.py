# SPDX-FileCopyrightText: 2019-2023 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only
"""Simulation of OPC UA PLC interface through shared memory

This module provides a simulated interface for a OPC UA PLC through
shared memory.  The OpcUaBase class allows creating and managing
shared OPC UA variables across different processes.
"""

# Built-in
import logging
import fcntl
import pathlib
from contextlib import contextmanager
from multiprocessing import shared_memory
from typing import Any, Optional


@contextmanager
def flocked(fd):
    """Context manager to lock the file descriptor.

    Locks the file descriptor on enter and releases it on exit.

    :param fd: The file descriptor to be locked.
    """
    fcntl.flock(fd, fcntl.LOCK_EX)
    try:
        yield
    finally:
        fcntl.flock(fd, fcntl.LOCK_UN)


class OpcUaBase:
    """OpcUa base class which should be extended

    This class should be extended to create shared variables that can be
    accessed and modified by different processes. It manages the shared
    memory and file locking to ensure safe access.

    Example:

    >>> class OpcUa(OpcUaBase):
    ...     HV_VoltageActual_GET = FloatVar()
    ...     HV_Start_CMD = BoolVar()
    ...     HV_ArcTripStatus_GET = IntVar()
    ...     LayerFeedParam = StructVar()

    Now variables can be read and written, like this:

    >>> opc = OpcUa("opcualib", "/dev/shm/opcualib")
    >>> opc.HV_VoltageActual_GET = 3.14
    >>> opc.HV_VoltageActual_GET
    3.14

    Other processes can attach and set the same variables, but should
    use `owner=False`.
    """

    variables: list[str] = []
    preplist: list[Any] = []

    def __init__(
        self, name: str, lockfile: Optional[pathlib.Path] = None, owner: bool = True
    ) -> None:
        """Initialization of OpcUaBase.

        :param name: Name of the shared memory block.
        :type name: str

        :param lockfile: Path to the lock file used for file locking.
            Used to get exclusive access to the shared memory.
        :type lockfile: pathlib.Path

        :param owner: If true, create and be the owner of the shared
            memory.  If false, attach to existing shared memory.
        :type owner: bool, optional
        """
        self.name = name
        if lockfile is None:
            self.lockfile = pathlib.Path(f"/dev/shm/{name}")
        else:
            self.lockfile = pathlib.Path(lockfile)
        self.owner = owner
        self.fh = None

        # E.g. "HV_VoltageActual_GET" -> 123 (index in ShareableList)
        self.name2index = {name: i for i, name in enumerate(self.variables)}

        self.log = logging.getLogger(self.__class__.__name__)

    def __enter__(self):
        if self.owner:  # Create list
            mem = shared_memory.ShareableList(self.preplist, name=self.name)
            for i in range(len(mem)):
                if isinstance(mem[i], bytes):
                    mem[i] = b""
                if isinstance(mem[i], str):
                    mem[i] = ""
        else:  # Attach to existing list
            mem = shared_memory.ShareableList(None, name=self.name)
        self.mem = mem
        self.fh = open(self.lockfile)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.fh:
            self.fh.close()
        self.mem.shm.close()
        if self.owner:
            # Only the owner should unlink the shared memory
            self.mem.shm.unlink()
        else:
            # unregister is needed to provent other processes from
            # unlinking the shared memory at exit.  We want processes
            # to be able to attach multiple times.

            # https://bugs.python.org/issue38119
            from multiprocessing.resource_tracker import unregister

            unregister(self.mem.shm._name, "shared_memory")

    def get(self, name: str) -> Any:
        """Get value of variable named `name`"""
        index = self.name2index[name]
        with flocked(self.fh):
            value = self.mem[index]
            self.log.debug("GET %s -> %r", name, value)
            return value

    def set(self, name: str, value: Any) -> None:
        """Set value of variable named `name`"""
        index = self.name2index[name]
        with flocked(self.fh):
            self.mem[index] = value
            self.log.debug("SET %s <- %r", name, value)


class BoolVar:
    def __init__(self) -> None:
        self._name = ""

    def __set_name__(self, owner, name: str):
        self._name = name
        owner.variables.append(name)
        owner.preplist.append(bool())

    def __get__(self, instance: OpcUaBase, owner) -> bool:
        if instance:
            # Return the actual value when accessed through instance:
            return instance.get(self._name)
        # Return the variable name as str when accessed through class:
        # E.g. OpcUa.HV_VoltageActual_GET -> "HV_VoltageActual_GET"
        return self._name

    def __set__(self, instance: OpcUaBase, value: bool) -> None:
        instance.set(self._name, value)


class IntVar:
    def __init__(self) -> None:
        self._name = ""

    def __set_name__(self, owner, name: str):
        self._name = name
        owner.variables.append(name)
        owner.preplist.append(int())

    def __get__(self, instance: OpcUaBase, owner) -> int:
        if instance:
            return instance.get(self._name)
        return self._name

    def __set__(self, instance: OpcUaBase, value: int) -> None:
        instance.set(self._name, value)


class FloatVar:
    def __init__(self) -> None:
        self._name = ""

    def __set_name__(self, owner, name: str):
        self._name = name
        owner.variables.append(name)
        owner.preplist.append(float())

    def __get__(self, instance: OpcUaBase, owner) -> float:
        if instance:
            return instance.get(self._name)
        return self._name

    def __set__(self, instance: OpcUaBase, value: float) -> None:
        instance.set(self._name, value)


class StringVar:
    def __init__(self, maxsize: int = 64) -> None:
        self._name = ""
        self.maxsize = maxsize

    def __set_name__(self, owner, name: str):
        self._name = name
        owner.variables.append(name)
        owner.preplist.append(str())
        owner.preplist.append(b" " * self.maxsize)  # Allocate space

    def __get__(self, instance: OpcUaBase, owner) -> str:
        if instance:
            return instance.get(self._name)
        return self._name

    def __set__(self, instance: OpcUaBase, value: str) -> None:
        instance.set(self._name, value)


class StructVar:
    def __init__(self, maxsize: int = 64) -> None:
        self._name = ""
        self.maxsize = maxsize

    def __set_name__(self, owner, name: str):
        self._name = name
        owner.variables.append(name)
        owner.preplist.append(b"\xff" * self.maxsize)  # Allocate space

    def __get__(self, instance: OpcUaBase, owner) -> bytes:
        if instance:
            data = instance.get(self._name)
            if data:
                # https://github.com/python/cpython/issues/106939
                return data[:-1]
            return data
        return self._name

    def __set__(self, instance: OpcUaBase, value: bytes) -> None:
        # https://github.com/python/cpython/issues/106939
        instance.set(self._name, value + b"\xff")
