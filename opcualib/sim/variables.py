# SPDX-FileCopyrightText: 2019-2023 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

# This module is auto-generated!

from .base import *


class OpcUa(OpcUaBase):
    HV_VoltageActual_GET = FloatVar()  # HV:VoltageActual
    HV_VoltageDemand_SET = FloatVar()  # HV:VoltageDemand
    HV_CurrentActual_GET = FloatVar()  # HV:CurrentActual, Beam:Current
    HV_CurrentLimit_SET = FloatVar()  # HV:CurrentLimit
    HV_Start_CMD = BoolVar()  # HV:Start
    HV_Fault_GET = BoolVar()  # HV:Fault
    HV_ArcTripStatus_GET = IntVar()  # HV:ArcTrip
    HV_SafetyInhibit_GET = BoolVar()  # HV:SafetyInhibit
    HV_PowerOK_GET = BoolVar()  # HV:PsuOn
    HV_PowerStatus_GET = BoolVar()  # HV:PsuStatus
    HV_PowerEnable_CMD = BoolVar()  # HV:PsuPower
    BC_CurrentDemand_GET = FloatVar()  # Beam:CurrentDemand
    BC_PatternActive_GET = (
        BoolVar()
    )  # Beam:PatternActiveStatus, Command:PatternActiveStatus
    BeamAmp_Enable_CMD = BoolVar()  # Beam:Amplifiers
    CoilSectionFan_SET = BoolVar()  # CoilSectionFan, CoilSectionFan
    CR_RegulatorEnable_SET = BoolVar()  # Regulator:Enable
    CR_RegulatorP_SET = FloatVar()  # Regulator:P
    CR_RegulatorI_SET = FloatVar()  # Regulator:I
    CR_RegulatorD_SET = FloatVar()  # Regulator:D
    CR_IdleThreshold_SET = FloatVar()  # Regulator:IdleThreshold
    CR_IdleCurrent_SET = FloatVar()  # Regulator:IdleCurrent
    CR_LaserPowerLimit_SET = FloatVar()  # Regulator:LaserPowerLimit
    LA_LaserPowerActual_GET = FloatVar()  # Regulator:LaserPowerActual
    CR_CurrentDemandOverride_SET = FloatVar()  # Regulator:CurrentDemandOverride
    CR_ATReadyStart_GET = IntVar()  # AutoTune:ReadyStart
    CR_ATStart_CMD = IntVar()  # AutoTune:Start
    CR_ATDone_GET = IntVar()  # AutoTune:Done
    CR_ATResultP_GET = IntVar()  # AutoTune:Result:P
    CR_ATResultI_GET = IntVar()  # AutoTune:Result:I
    CR_ATResultD_GET = IntVar()  # AutoTune:Result:D
    CR_ATHysteresis_SET = IntVar()  # AutoTune:Hysteresis
    CR_ATCalcGain_SET = IntVar()  # AutoTune:CalcGain
    CR_ATCurret_SET = IntVar()  # AutoTune:Current
    BuildEncoder_GET = FloatVar()  # Servo:BuildPosition
    PowderEncoder_GET = FloatVar()  # Servo:PowderPosition
    RakeEncoder_GET = FloatVar()  # Servo:RecoaterPosition
    RakeSpeed_SET = FloatVar()  # Servo:RecoaterMoveSpeed
    BuildAbsolutePosition_SET = IntVar()  # Servo:BuildABSMovePosition
    PowderAbsolutePosition_SET = IntVar()  # Servo:PowderABSMovePosition
    RakeAbsolutePosition_SET = IntVar()  # Servo:RecoaterABSMovePosition
    BuildEnable_SET = IntVar()  # Servo:BuildEnable
    PowderEnable_SET = IntVar()  # Servo:PowderEnable
    RakeEnable_SET = IntVar()  # Servo:RecoaterEnable
    BuildStatus_GET = BoolVar()  # Servo:BuildStatus
    PowderStatus_GET = BoolVar()  # Servo:PowderStatus
    RakeStatus_GET = BoolVar()  # Servo:RecoaterStatus
    AllServoReset_SET = IntVar()  # Servo:AllReset, Command:ResetAll
    BuildZeroPosition_SET = BoolVar()  # Servo:BuildZeroPos
    PowderZeroPosition_SET = BoolVar()  # Servo:PowderZeroPos
    RakeZeroPosition_SET = BoolVar()  # Servo:RakeZeroPos
    BuildHomeTrq_SET = FloatVar()  # Servo:BuildHomingTorque
    BuildHomeOffs_SET = FloatVar()  # Servo:BuildHomingOffset
    PowderHomeTrq_SET = FloatVar()  # Servo:PowderHomingTorque
    PowderHomeOffs_SET = FloatVar()  # Servo:PowderHomingOffset
    BuildHomingStatus_GET = BoolVar()  # Servo:BuildHomingStatus
    PowderHomingStatus_GET = BoolVar()  # Servo:PowderHomingStatus
    WallShutterOpen_SET = BoolVar()  # WallShutter:Open
    WallShutterSensor_GET = BoolVar()  # WallShutter:IsOpen
    BuildHome_CMD = BoolVar()  # Command:BuildHoming
    PowderHome_CMD = BoolVar()  # Command:PowderHoming
    X_GET = IntVar()  # Command:SafetyServoInhibit
    LayerFeedParam = StructVar()  # Command:LayerFeed
    LayerFeedStatus = StructVar()  # Command:LayerFeedStatus
    PatternActive = IntVar()  # Command:TriggerSource
    BuildAbsoluteMove_CMD = IntVar()  # Command:BuildAbsoluteMove
    PowderAbsoluteMove_CMD = IntVar()  # Command:PowderAbsoluteMove
    RakeAbsoluteMove_CMD = IntVar()  # Command:RecoaterAbsoluteMove
    PH_AutoLayerFeedControl_SET = BoolVar()  # ProHeat:AutomaticLayerFeedControl
    PH_SyncPointControl_SET = BoolVar()  # ProHeat:SyncPointControl
    PH_IsInstalled_SET = BoolVar()  # ProHeat:IsInstalled
    PH_Man_ToHeatPos_CMD = BoolVar()  # ProHeat:ManualControlHeating
    PH_Man_ToHomePos_CMD = BoolVar()  # ProHeat:ManualControlHome
    PH_AtHomePos_GET = BoolVar()  # ProHeat:AtHomePosition
    PH_AtHeatPos_GET = BoolVar()  # ProHeat:AtHeatingPosition
    PH_MoveNotAllowed_GET = BoolVar()  # ProHeat:ActivationError
    PH_SyncPoint_GET = BoolVar()  # ProHeat:SyncPoint
    eGunPressure_GET = FloatVar()  # Pressure:Gun
    BuildPressure_GET = FloatVar()  # Pressure:Build
    PowderPressure_GET = FloatVar()  # Pressure:Powder
    RakeBoxPressure_GET = FloatVar()  # Pressure:Backing
    OpenGunValve_SET = BoolVar()  # Ventilation:GunEnable
    OpenBuildValve_SET = BoolVar()  # Ventilation:BuildEnable
    OpenPowderValve_SET = BoolVar()  # Ventilation:PowderEnable
    StartForePump_SET = BoolVar()  # BackingPump:Enable
    StartGunTurbo_SET = BoolVar()  # TurboPump:GunEnable
    StartBuildTurbo_SET = BoolVar()  # TurboPump:BuildEnable
    StartPowderTurbo_SET = BoolVar()  # TurboPump:PowderEnable
    ActGunTurboFreq_GET = FloatVar()  # TurboPump:GunFreq
    ActBuildTurboFreq_GET = FloatVar()  # TurboPump:BuildFreq
    ActPowderTurboFreq_GET = FloatVar()  # TurboPump:PowderFreq
    ActGunTurboTemp_GET = IntVar()  # TurboPump:GunTemp
    ActBuildTurboTemp_GET = IntVar()  # TurboPump:BuildTemp
    ActPowderTurboTemp_GET = IntVar()  # TurboPump:PowderTemp
    ActGunTurboCurrent_GET = IntVar()  # TurboPump:GunCurrent
    ActBuildTurboCurrent_GET = IntVar()  # TurboPump:BuildCurrent
    ActPowderTurboCurrent_GET = IntVar()  # TurboPump:PowderCurrent
    TempSensor1_GET = FloatVar()  # Temperature:Sensor1
    TempSensor2_GET = FloatVar()  # Temperature:Sensor2
    TempSensor3_GET = FloatVar()  # Temperature:Sensor3
    TempSensor4_GET = FloatVar()  # Temperature:Sensor4
    VacuumLamp_SET = BoolVar()  # VacuumLamp
