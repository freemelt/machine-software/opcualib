# SPDX-FileCopyrightText: 2019-2023 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only
"""Simulation of OPC UA PLC interface through shared memory"""

# Built-in
import argparse
import asyncio
import collections
import importlib
import logging
import pathlib
import re

# Third-party
import yaml

# Package
from . import extract_leafs


def load_variables(yamlfiles: list[pathlib.Path]) -> dict:
    output = collections.defaultdict(dict)
    for file in yamlfiles:
        # Find all python files recursively in same directory as the
        # yaml. Find all lines containing "opc".
        lines = list()
        for pyfile in file.parent.glob("**/*.py"):
            text = pyfile.read_text()
            for line in text.splitlines():
                if "opc" in line:
                    lines.append(line)

        # Build the `output` structure of OPC variables, aliases,
        # indexes and types.
        with open(file) as fh:
            cfg = yaml.safe_load(fh)
            del cfg["OPC"]["Namespace"]  # Not useful with extract_leafs
            del cfg["OPC"]["Base"]  # Not useful with extract_leafs
            for alias, name in extract_leafs("", cfg["OPC"]):
                # The same variable name could have different aliases
                # in different yaml config files. We use a list to
                # store all of them.
                al = output[name].setdefault("alias", list())
                al.append(alias)

                # Guess type based on usage in Python code. Not ideal.
                for line in lines:
                    if alias in line:
                        pattern = rf"_(\w+)\(.{alias}"
                        if match := re.search(pattern, line):
                            output[name]["type"] = match.groups()[0]
                            break
                else:
                    output[name]["type"] = "int"  # Default to int
    return output


def write_variables_module(variables: dict) -> None:
    out = pathlib.Path(__file__).parent / "variables.py"
    with open(out, "w") as fh:
        fh.write("# This module is auto-generated!\n\n")
        fh.write("from .base import *\n\n")
        fh.write("class OpcUa(OpcUaBase):\n")
        for key, value in variables.items():
            fh.write(f"    {key} = {value['type'].title()}Var()")
            fh.write(f"  # {', '.join(value['alias'])}\n")


async def main(args: argparse.Namespace) -> None:
    if args.opc_config:
        if input("This will overwrite variables.py. Are you sure? (y/n)") != "y":
            print("Aborted.")
            return
        vars = load_variables(args.opc_config)
        write_variables_module(vars)
        print("Done.")
        return

    # Dynamically load the module implementing the simulation:
    sim_module = importlib.import_module(args.simulation_module)

    with sim_module.Simulation(
        name=args.name, lockfile=args.lockfile, owner=True
    ) as sim:
        await sim.run()


parser = argparse.ArgumentParser()
parser.add_argument(
    "--name",
    default="opcualib",
    help="Name of shared memory (default: %(default)s)",
)
parser.add_argument(
    "--lockfile",
    metavar="PATH",
    type=pathlib.Path,
    default="/dev/shm/opcualib",
    help="Path to lockfile for access to shared memory",
)
parser.add_argument(
    "--opc-config",
    metavar="PATH",
    nargs="+",
    type=pathlib.Path,
    help="Paths to OPC yaml config files",
)
parser.add_argument(
    "--simulation-module",
    "-m",
    metavar="module",
    default="simulation",
    help=(
        "Python module which implements the simulation. "
        "The module will be imported and the `Simulation` "
        "object will be started (default: %(default)s)."
    ),
)
parser.add_argument(
    "-l",
    "--loglevel",
    choices=["debug", "info", "warning", "error", "critical"],
    default="info",
    help="Set log level (default: %(default)s)",
)

if __name__ == "__main__":
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel.upper())
    try:
        asyncio.run(main(args))
    except KeyboardInterrupt:
        print("Bye")
