# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

import sys
import setuptools

sys.path.append("opcualib")
from _version import __version__

sys.path.remove("opcualib")

with open("README.md", "r") as fh:
    description = fh.read()

setuptools.setup(
    name="opcualib",
    version=__version__,
    author="Freemelt AB",
    author_email="opensource@freemelt.com",
    description="A wrapper to improve freeopcualib",
    long_description=description,
    url="https://gitlab.com/freemelt/machine-software/opcualib",
    packages=setuptools.find_packages(exclude=["tests"]),
    install_requires=["asyncua>=0.6.1", "cryptography>=2.6.1"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: OS Independent",
    ],
)
