<!--
SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: LGPL-3.0-only
-->

# API for wrapper OPC UA lib

# NOTE: This documentation applies to the deprecated OPCWrapper.

## Functionality
- Connect to server
- Disconnect from server
- Set Root object
- Set value on Node
- Get value from Node
- Set debug level on prints

### Connect to server

```python
def connect(server_URI)
```

- Could not connect raises `ServerConnectException`

### Disconnect from server

```python
def disonnect()
```

### Set Base Path

Sets an path which all subsequent calls
use as base. Simplifies getting and setting variables.

```python
def set_base_path(path)
```

Returne BOOL as Succes/Failure, in case the path does not exist on the server.

### Set value on Node

Path is a `|` delimited string with OPC-UA NodeIDs.
E.g `GlobalVars|LayerFeedParam`, which uses the base object
`Objects|new_Controller_2` making the full path

`Objects|new_Controller_2|GlobalVars|LayerFeedParam`

```python
def set_int(path, value)
def set_float(path, value)
def set_bool(path, value)
def set_string(path, value)
def set_struct(path, value) # TBD
```

- Wrong type raises `WrongValueException`
- Missing Node raises `MissingNodeException`

### Get value from Node

```python
def get_int(path)
def get_float(path)
def get_bool(path)
def get_string(path)
def get_struct(path) # TBD
```

Returns value on Success.

- Missing Node raises `MissingNodeException`
- Node does not have value raises `BadAttributeIdInvalid`

### Set Debug log level

Sets `logger` log level for stdout prints.

```python
def set_debug_level(lvl)
```
