.. configurator documentation master file, created by
   sphinx-quickstart on Wed Jun 12 11:13:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. mdinclude:: ../../README.md


Module Reference
----------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autoclass:: opcualib.SyncClient
   :members:

.. autoclass:: opcualib.OPCWrapper
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
