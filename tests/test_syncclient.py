# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

"""Test suite for SyncClient

NOTE: You need a connected PLC in order to run these tests.
      Make sure you have installed the correct version of asyncua==0.6.1

"""
# Built-in
import unittest
import os

# PyPI
from asyncua.ua.uaerrors import BadNoMatch, BadTypeMismatch
from asyncua.ua.uatypes import ExtensionObject, NodeId, NodeIdType

# Project
import opcualib

# Environment settings
HOSTNAME = "127.0.0.1"
PORT = 4840
NAMESPACE = "urn:PLC:OPCUaServer:FactoryAutomation"
BASE = "Objects|PLC|GlobalVars"

# Example variables used by test
FLOAT_VAR = "EventArray_GET"


@unittest.skipIf("GITLAB_CI" in os.environ, "Needs a connected PLC")
class TestSyncClient(unittest.TestCase):
    def setUp(self):
        self.client = opcualib.SyncClient(HOSTNAME, PORT, NAMESPACE, BASE)
        self.client.connect()

    def test_get(self):
        self.client.get_value(FLOAT_VAR)

    def test_set(self):
        self.client.set_float(FLOAT_VAR, 3.14)
        value = self.client.get_value(FLOAT_VAR)
        self.assertAlmostEqual(value, 3.14, places=5)

    def test_name_error_get(self):
        with self.assertRaises(BadNoMatch):
            self.client.get_value("DoesNotExistVariable_GET")

    def test_name_error_set(self):
        with self.assertRaises(BadNoMatch):
            self.client.set_float("DoesNotExistVariable_SET", 81611)

    def test_type_error_set(self):
        with self.assertRaises(BadTypeMismatch):
            self.client.set_string(FLOAT_VAR, "asdf")

    def test_as_dict(self):
        self.client.set_float(FLOAT_VAR, 1)
        d = self.client.get_as_dict([FLOAT_VAR])
        self.assertDictEqual(d, {FLOAT_VAR: 1})

    def tearDown(self):
        self.client.shutdown()


if __name__ == "__main__":
    import logging
    import time

    logging.basicConfig(
        level=logging.DEBUG, format="%(created)f %(levelname)s %(name)s %(message)s"
    )
    logging.getLogger("asyncua").setLevel(logging.INFO)

    with opcualib.SyncClient(timeout=1) as client:

        # breakpoint()
        # resp = client.get_value('ArrayTest_GET')
        # resp = client.get_value('StructTest_GET')
        resp = client.get_value("EventArray_GET")
        # resp = client.get_value('ArrayStructTest_GET')
        print(resp)

        # ext = ExtensionObject()
        # # The namespace idx is assumed to the same for the
        # # extension object as it is for the variables
        # # 5002 is the extension object type used for LayerFeedParams
        # ext.TypeId = NodeId(
        #     namespaceidx=client.namespace_idx,
        #     identifier=5002,
        #     nodeidtype=NodeIdType.FourByte)
        # ext.Body = b'asdf'
        # ext.Encoding = 1
