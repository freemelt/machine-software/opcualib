<!--
SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: LGPL-3.0-only
-->

# OPC-UA Python interface

Use `opcualib.SyncClient` to interface with the PLC.

```python

# Use as a context manager:

>>> with SyncClient() as client:
>>>     value = client.get_float('HV_VoltageDemand_GET')
>>>     print(value)

# Or the procedural way:

>>> client = SyncClient()
>>> client.connect()
>>> value = client.get_float('HV_VoltageDemand_GET')
>>> print(value)
>>> client.shutdown()
```

The public methods are:

    Start/stop:
    * connect
    * shutdown

    Getters:
    * get_value - regardless of type
    * get_as_dict
    * get_int
    * get_uint
    * get_float
    * get_double
    * get_bool
    * get_string
    * get_struct

    Setters:
    * set_value - regardless of type
    * set_int
    * set_uint
    * set_float
    * set_double
    * set_bool
    * set_string
    * set_struct


# License
Copyright © 2019- 2021 Freemelt AB <opensource@freemelt.com>
The OPCUAlib is under [GNU Lesser General Public License, Version 3](https://www.gnu.org/licenses/lgpl-3.0.en.html)

